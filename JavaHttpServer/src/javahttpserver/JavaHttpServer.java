/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javahttpserver;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Will Rod
 */
public class JavaHttpServer implements Runnable {
    static final File WEB_ROOT = new File(".");
    static final int PORT = 9090;
    static final util util= new util();
    
    static final boolean verbose = true;
    private Socket connect;

    public JavaHttpServer(Socket connect) {
        this.connect = connect;
    }

    public static void main(String[] args){
        try {
            ServerSocket serverConnect = new ServerSocket(PORT);
            System.out.println("Server started... \n Listening for connection on port: "+PORT);
            while(true){
                JavaHttpServer myServer = new JavaHttpServer(serverConnect.accept());
				
                if (verbose) {
                        System.out.println("Connecton opened. (" + new Date() + ")");
                }
                Thread thread = new Thread(myServer);
                thread.start();
            }
        } catch (IOException ex) {
            System.err.println("Server Connection Error: "+ex.getMessage());
            Logger.getLogger(JavaHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;
        BufferedOutputStream dataOut = null;
        String fileRequested = null;
        try {
            in = new BufferedReader(new InputStreamReader(connect.getInputStream()));            
            out = new PrintWriter(connect.getOutputStream());
            dataOut = new BufferedOutputStream(connect.getOutputStream());
            String input = null;
            
            input = in.readLine();
            System.out.println("input:<<<<<<<<<<<<<<<< "+input);
            StringTokenizer parse = new StringTokenizer(input);
            String method = parse.nextToken().toUpperCase();
            fileRequested = parse.nextToken().toLowerCase();            
            if (!method.equals("GET")  &&  !method.equals("HEAD") && !method.equals("PUT") && !method.equals("POST")) {
                methodNotSupported(method,out,dataOut);
            }else {
                System.out.println("----------- fileRequested "+fileRequested);
                if (fileRequested.endsWith("/")) {
                        //fileRequested += DEFAULT_FILE;
                }
                System.out.println("----------- fileRequested2 "+fileRequested);                
                String content = getContentType(fileRequested);
                if (method.equals("GET")) { // GET method so we return content                    
                    String msg="",tags="";                    
                    while (msg.equals("") || tags.equals("")) {
                        input = in.readLine();
                        if(input.contains("msg")){
                            msg = input.substring(13,input.length()-2);
                        }
                        if(input.contains("tags")){
                            tags = input.substring(13,input.length()-1);
                        }
                    }                    
                    String response="";
                    String[] parts = fileRequested.split("/");
                    long id = 0;
                    String tag = "";                    
                    try {
                        id = Long.parseLong(parts[2]);
                        response = util.findMessage(id);
                    } catch (Exception e) {
                        tag = parts[2];
                        response = util.findMessage(tag);
                    }       
                    System.out.println("id ++++++++++++ "+id);
                    if(parts[1].equals("message") && !msg.equals("") && !tags.equals("")){
                        
                        if(!response.equals("")){
                            out.println("HTTP/1.1 200 OK");
                        }else{
                            out.println("HTTP/1.1 402 OK");
                            response = "{'mensaje':'ha ocurrido un error al leer el mensaje'}";
                        }
                        out.println("Server: Java HTTP Server: 1.0");
                        out.println("Date: " + new Date());
                        out.println("Content-type: " + content);
                        out.println("Content-length: " + response.length());
                        out.println();
                        out.flush();

                        dataOut.write(response.getBytes(), 0, response.length());
                        dataOut.flush();
                    }else{
                        methodNotSupported(method,out,dataOut);
                    }                                                         
                }else if(method.equals("PUT")){
                    String name="",pass="";                    
                    while(name.equals("") || pass.equals("")) {
                        input = in.readLine();
                        if(input.contains("name")){
                            name = input.substring(13,input.length()-2);                            
                        }
                        if(input.contains("pass")){
                            pass = input.substring(13,input.length()-1);                            
                        }
                        System.out.println("input++++++++++++++ "+input);
                    }                   
                    String response;
                    String[] parts = fileRequested.split("/");                    
                    if(parts[1].equals("credencial") && !name.equals("") && !pass.equals("")){                        
                        if(util.validarionClave(name,pass)){
                            out.println("HTTP/1.1 403 OK");
                            response = "{'mensaje':'esta clave se encuentra repetida'}";
                        }else{
                            response = "{'mensaje':'esta clave a sido almacenada'}";
                            out.println("HTTP/1.1 204 OK");
                        }                    
                        out.println("Server: Java HTTP Server: 1.0");
                        out.println("Date: " + new Date());
                        out.println("Content-type: " + content);
                        out.println("Content-length: " + response.length());
                        out.println();
                        out.flush();

                        dataOut.write(response.getBytes(), 0, response.length());
                        dataOut.flush();
                    }else{
                        methodNotSupported(method,out,dataOut);
                    }                    
                }else if(method.equals("POST")){
                    String msg="",tags="";
                    
                    while (msg.equals("") || tags.equals("")) {
                        input = in.readLine();
                        if(input.contains("msg")){
                            msg = input.substring(12,input.length()-2);
                        }
                        if(input.contains("tags")){
                            tags = input.substring(13,input.length()-1);
                        }
                    }                    
                    String response="";
                    String[] parts = fileRequested.split("/");                    
                    if(parts[1].equals("message") && !msg.equals("") && !tags.equals("")){
                        long id_datos = util.saveMessage(msg,tags);                        
                        if(id_datos>0){
                            out.println("HTTP/1.1 200 OK");
                            response = "{'mensaje':"+id_datos+"}";
                        }else{
                            out.println("HTTP/1.1 402 OK");
                            response = "{'mensaje':'ha ocurrido un error al guardar el mensaje'}";
                        }
                        out.println("Server: Java HTTP Server: 1.0");
                        out.println("Date: " + new Date());
                        out.println("Content-type: " + content);
                        out.println("Content-length: " + response.length());
                        out.println();
                        out.flush();

                        dataOut.write(response.getBytes(), 0, response.length());
                        dataOut.flush();
                    }else{
                        methodNotSupported(method,out,dataOut);
                    }
                }

                if (verbose) {
                        System.out.println("File " + fileRequested + " of type " + content + " returned");
                }
            }
        } catch (FileNotFoundException fnfe) {
            try {
                fileNotFound(out, dataOut, fileRequested);
            } catch (IOException ioe) {
                System.err.println("Error with file not found exception : " + ioe.getMessage());
            }
        }catch (IOException ex) {
            Logger.getLogger(JavaHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            try {
                    in.close();
                    out.close();
                    dataOut.close();
                    connect.close(); // we close socket connection
            } catch (Exception e) {
                    System.err.println("Error closing stream : " + e.getMessage());
            } 

            if (verbose) {
                    System.out.println("Connection closed.\n");
            }
        }
        
    }
    
    private String getContentType(String fileRequested) {
        if (fileRequested.endsWith(".htm")  ||  fileRequested.endsWith(".html"))
            return "text/html";
        else
            return "application/json; charset=%s";
    }
    
    private void fileNotFound(PrintWriter out, OutputStream dataOut, String fileRequested) throws IOException {                
        String response = "{mensaje:'FileNotFound'}";
        String content = "text/html";        

        out.println("HTTP/1.1 404 File Not Found");
        out.println("Server: Java HTTP Server : 1.0");
        out.println("Date: " + new Date());
        out.println("Content-type: " + content);
        out.println("Content-length: " + response.length());
        out.println(); // blank line between headers and content, very important !
        out.flush(); // flush character output stream buffer

        dataOut.write(response.getBytes(), 0, response.length());
        dataOut.flush();

        if (verbose) {
                System.out.println("File " + fileRequested + " not found");
        }
    }
    
    private void methodNotSupported(String method,PrintWriter out,BufferedOutputStream dataOut) throws IOException{        
        if (verbose) {
            System.out.println("501 Not Implemented : " + method + " method.");
        }
        String response = "{mensaje:'501 Not Implemented : " + method + " method.'}";
        String contentMimeType = "application/json; charset=%s";
        out.println("HTTP/1.1 501 Not Implemented");
        out.println("Server: Java HTTP Server from SSaurel : 1.0");
        out.println("Date: " + new Date());
        out.println("Content-type: " + contentMimeType);
        out.println("Content-length: " + response.length());
        out.println();
        out.flush();
        dataOut.write(response.getBytes(), 0, response.length());
        dataOut.flush();
    }
    
}
