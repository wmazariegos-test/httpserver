package javahttpserver;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Will Rod
 */
public class util {
    List<Claves> claves = new ArrayList();
    List<Mensaje> mensajes = new ArrayList();
    public util() {        
    }
    
    public boolean validarionClave(String name, String pass){        
        Claves clave = new Claves();
        clave.setNombre(name);
        clave.setClave(pass);        
       if(findClaves(name)){           
           return true;
       }else{           
           claves.add(clave);
           return false;
       }        
    }
    public List<Claves> getClaves() {
        return claves;
    }

    public void setClaves(List<Claves> claves) {
        this.claves = claves;
    }
    private boolean findClaves(String name){
        Iterator<Claves> it = claves.iterator();
        boolean result = false;        
        while(it.hasNext() && result == false){
            Claves c = it.next();            
            if(c.getNombre().equals(name)){
                result = true;
            }
        }        
        return result;
    }

    public long saveMessage(String msg, String tags) {
        Mensaje mensaje = new Mensaje();
        System.out.println("guardando mensaje "+msg);
        System.out.println("guardando tag "+tags);
        mensaje.setMsg(msg);
        mensaje.setTag(tags);
        mensaje.setId(new Date().getTime());
        mensajes.add(mensaje);
        return mensaje.getId();
    }

    String findMessage(long id) {
        Iterator<Mensaje> it = mensajes.iterator();
        String result = "";
        while(it.hasNext()){
            Mensaje c = it.next();
            System.out.println("getId()"+c.getId());
            if(c.getId() == id){
                result = "{'msg': "+c.getMsg()+"',"
                        + "'tags': "+c.getTag()+"}";
            }
        }
        System.out.println("buscando mensaje: "+id);
        System.out.println("mensaje encontrado: "+result);
        return result;
    }

    String findMessage(String tag) {
        Iterator<Mensaje> it = mensajes.iterator();
        String result = "";
        int contador = 0;
        while(it.hasNext()){
            Mensaje c = it.next();
            if(c.getTag().equals(tag)){
                if (contador == 0){
                    result = "{'msg': "+c.getMsg()+"',"
                        + "'tags': "+c.getTag()+"}";
                }else{
                    result = result +",{'msg': "+c.getMsg()+"',"
                        + "'tags': "+c.getTag()+"}";
                }
                
            }
        }
        System.out.println("buscando mensaje: "+tag);
        System.out.println("mensaje encontrado: "+result);
        return result;
    }
}
